import java.lang.Math;
import java.util.Set;
import java.util.Comparator;
import static java.util.Collections.sort;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Iterator;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.BufferedWriter;

import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import utfpr.ct.dainf.if62c.pratica.ContadorPalavras;

public class Pratica72
{
	public static void main(String[] args) throws FileNotFoundException, IOException
	{
		Scanner s = new Scanner(System.in);
		System.out.println("Digite o caminho para o arquivo: ");
		String path = s.nextLine();
		while (path.isEmpty())
			path = s.nextLine();

			FileReader fr = new FileReader(path);
		ContadorPalavras cp = new ContadorPalavras(fr);
		HashMap<String, Integer> contador = cp.getPalavras();

		LinkedHashMap<String, Integer> ordenado = sort_hashmap(contador);
		for (String key : ordenado.keySet()) {
			Integer value = ordenado.get(key);
			System.out.println(key + " = " + value);
		}

		BufferedWriter bw = new BufferedWriter(new FileWriter(path + ".out"));
		for (Map.Entry<String, Integer> entry : ordenado.entrySet()) {
			bw.write(entry.getKey() + "," + entry.getValue());
			bw.newLine();
		}

		bw.close();
	}

	public static LinkedHashMap<String, Integer> sort_hashmap (HashMap<String, Integer> contador) {
		List<Integer> palavras = new ArrayList<Integer>(contador.values());
		Collections.sort(palavras, Collections.reverseOrder());

		LinkedHashMap<String, Integer> ordenado = new LinkedHashMap<>();
    for (Integer count : palavras)
    {
			for (Map.Entry<String, Integer> entry : contador.entrySet()) {
        if (entry.getValue() == count) {
						ordenado.put(entry.getKey(), entry.getValue());
						contador.remove(entry.getKey().toString());
						break;
        }
	    }
    }

		return ordenado;
	}
}
