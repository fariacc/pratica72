package utfpr.ct.dainf.if62c.pratica;

import java.lang.Math;
import java.util.HashMap;
import java.util.Set;
import java.util.Comparator;
import static java.util.Collections.sort;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ContadorPalavras
{
	private BufferedReader reader;

	public ContadorPalavras (FileReader arquivo) {
		reader = new BufferedReader(arquivo);
	}

	public HashMap<String, Integer> getPalavras() throws IOException {
		HashMap<String, Integer> contador = new HashMap<>();
		String line = "";

		line = reader.readLine();

		while(line != null && !line.isEmpty()) {
			String[] words = line.split("[\\p{Punct}\\s]+");

			for (String word:words){
				Integer quant = contador.get(word);
				if (quant == null) {
					contador.put(word, 1);
				} else {
					contador.put(word, quant + 1);
				}
			}
			line = reader.readLine();
		}

		reader.close();
		return contador;
	}
}
